package com.transperfect.issuemanager.test;

import com.google.common.collect.Lists;
import com.transperfect.issuemanager.model.Issue;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.AssertTrue;
import java.util.LinkedHashMap;
import java.util.List;

import static org.junit.Assert.*;

public class ApplicationTestClient {

    public static final String SERVICE_URI = "http://localhost:8080/api";
    public static final int TIMEOUT = 6000;

    @Test
    public void createRepoIssue(){
        RestTemplate restTemplate = new RestTemplate();
        String owner = "Google";
        String repo = "tensorflow";
        long issueNumber = 125L;
        String title = "All is wrong 2";
        String body = "When I enter to the program, It crashes 2";
        String state = "Done";
        String repositoryUrl = "";

        Issue issue = new Issue(repositoryUrl,issueNumber, state, title, body);

        LinkedHashMap<String, Object> issuesMap = restTemplate.postForObject(SERVICE_URI +"/repos/"+ owner + "/" + repo + "/issues",issue,LinkedHashMap.class);


        assertEquals(issuesMap.get("title"), title);
    }

    /* GET */
    @SuppressWarnings("unchecked")
    @Test
    public void listAllIssues(){
        RestTemplate restTemplate = new RestTemplate();
        List<LinkedHashMap<String, Object>> issuesMap = restTemplate.getForObject(SERVICE_URI +"/issues/", List.class);

        assertEquals(3, issuesMap.size());
    }

    @Test
    public void listOwnerRepoIssues(){
        RestTemplate restTemplate = new RestTemplate();
        String owner = "Google";
        String repo = "tensorflow";
        List<LinkedHashMap<String, Object>> issuesMap = restTemplate.getForObject(SERVICE_URI +"/repos/"+ owner + "/" + repo + "/issues", List.class);

        assertEquals(2, issuesMap.size());
    }

    @Test
    public void listOwnerRepoIssue(){
        RestTemplate restTemplate = new RestTemplate();
        String owner = "Google";
        String repo = "tensorflow";
        long issueNumber = 124L;
        String title = "All is wrong";
        LinkedHashMap<String, Object> issuesMap = restTemplate.getForObject(SERVICE_URI +"/repos/"+ owner + "/" + repo + "/issues/" + String.valueOf(issueNumber), LinkedHashMap.class);

        assertEquals(issuesMap.get("title"), title);
    }

    @Test
    public void editRepoIssue(){
//        RestTemplate restTemplate = new RestTemplate();
        String owner = "Google";
        String repo = "tensorflow";
        long issueNumber = 125L;
        String title = "All is wrong 56";
        String body = "When I enter to the program, It crashes 2";
        Issue issue = new Issue(issueNumber, title, body);

        RestTemplate restTemplate = new RestTemplate();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        requestFactory.setConnectTimeout(TIMEOUT);
        requestFactory.setReadTimeout(TIMEOUT);

        restTemplate.setRequestFactory(requestFactory);

        LinkedHashMap<String, Object> issuesMap = restTemplate.patchForObject(SERVICE_URI +"/repos/"+ owner + "/" + repo + "/issues/" + issueNumber,issue,LinkedHashMap.class);


        assertEquals(issuesMap.get("title"), title);
    }
}
