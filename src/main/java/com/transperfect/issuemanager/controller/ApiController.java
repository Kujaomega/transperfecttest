package com.transperfect.issuemanager.controller;

import com.transperfect.issuemanager.model.Issue;
import com.transperfect.issuemanager.service.IssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@SpringBootApplication(scanBasePackages={"com.websystique.springboot"})
@ComponentScan(basePackages = {"com.transperfect.issuemanager.service"})
@RequestMapping("/api")
public class ApiController {

    @Autowired
    IssueService issueService;

    @RequestMapping(value = "/issues/", method = RequestMethod.GET)
    public ResponseEntity<List<Issue>> listAllIssues() {
        List<Issue> issues = issueService.findAllIssues();
        if (issues.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Issue>>(issues, HttpStatus.OK);
    }

    @RequestMapping(value="/repos/{owner}/{repo}/issues", method = RequestMethod.GET)
    public ResponseEntity<List<Issue>> listOwnerRepoIssues(@PathVariable("owner") String ownerName, @PathVariable("repo") String repoName) {
        List<Issue> repositories = issueService.findAllOwnerRepoIssues(ownerName, repoName);
        if (repositories.isEmpty()) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<List<Issue>>(repositories, HttpStatus.OK);
    }

    @RequestMapping(value="/repos/{owner}/{repo}/issues/{number}", method = RequestMethod.GET)
    public ResponseEntity<Issue> listOwnerRepoIssue(@PathVariable("owner") String ownerName, @PathVariable("repo") String repoName, @PathVariable("number") long issueNumber) {
        Issue issue = issueService.findOwnerRepoIssue(ownerName, repoName, issueNumber);
        if (issue == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Issue>(issue, HttpStatus.OK);
    }

    @RequestMapping(value="/repos/{owner}/{repo}/issues", method = RequestMethod.POST)
    public ResponseEntity createIssue(@PathVariable("owner") String ownerName, @PathVariable("repo") String repoName, @RequestBody Issue issue) {
        Issue createdIssue = issueService.createIssue(issue, ownerName, repoName);
        if (createdIssue == null) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<Issue>(createdIssue, HttpStatus.OK);
    }

    @RequestMapping(value="/repos/{owner}/{repo}/issues/{number}", method = RequestMethod.PATCH)
    public ResponseEntity<Issue> editOwnerRepoIssue(@PathVariable("owner") String ownerName,
                                                    @PathVariable("repo") String repoName,
                                                    @PathVariable("number") long issueNumber,
                                                    @RequestBody Issue issue) {
        Issue issueEdited = issueService.editIssue(issue, ownerName, repoName, issueNumber);
        if (issueEdited == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Issue>(issueEdited, HttpStatus.OK);
    }
}
