package com.transperfect.issuemanager.service;

import com.transperfect.issuemanager.model.Issue;
import com.transperfect.issuemanager.model.Owner;
import com.transperfect.issuemanager.model.Repository;
import com.transperfect.issuemanager.model.User;

import java.util.List;


public interface IssueService {
    List<Issue> findAllIssues();
    List<Issue> findAllOwnerRepoIssues(String ownerName, String repositoryName);
    Issue findOwnerRepoIssue(String ownerName, String repositoryName, long issueNumber);
    Issue createIssue(Issue issue, String ownerName, String repoName);
    Issue editIssue(Issue issue, String ownerName, String repoName, long issueNumber);
    List<Repository> findRepositoriesByOwnerName(String name);
}
