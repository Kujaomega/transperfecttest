package com.transperfect.issuemanager.service;

import com.google.common.collect.Lists;
import com.transperfect.issuemanager.model.Issue;
import com.transperfect.issuemanager.model.Owner;
import com.transperfect.issuemanager.model.Repository;
import com.transperfect.issuemanager.model.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


@Service
public class IssueServiceImpl implements IssueService {

    private static final AtomicLong counter = new AtomicLong();

    private static List<Issue> issues;

    static{
        issues = populateDummyIssues();
    }


    @Override
    public List<Issue> findAllIssues() {
        return issues;
    }

    @Override
    public List<Issue> findAllOwnerRepoIssues(String ownerName, String repositoryName) {
        List<Issue> issueList = Lists.newArrayList();
        for(Issue issue: issues){
            if(issue.getRepository().getOwner().getLogin().equals(ownerName)){
                if(issue.getRepository().getName().equals(repositoryName)){
                    issueList.add(issue);
                }
            }

        }
        return issueList;
    }

    @Override
    public Issue findOwnerRepoIssue(String ownerName, String repositoryName, long issueNumber) {
        for(Issue issue: issues){
            if(issue.getRepository().getOwner().getLogin().equals(ownerName)){
                if(issue.getRepository().getName().equals(repositoryName)){
                    if(issue.getNumber() == issueNumber){
                        return issue;
                    }
                }
            }

        }
        return null;
    }

    @Override
    public Issue createIssue(Issue issue, String ownerName, String repoName) {
        List<Repository> listRepository = findRepositoriesByOwnerName(ownerName);
        for(Repository repository: listRepository){
            if(repository.getName().equals(repoName)){
                issue.setRepository(repository);
            }
        }
        issues.add(issue);
        return issue;
    }

    @Override
    public Issue editIssue(Issue issue, String ownerName, String repoName, long issueNumber) {
        Issue issueFound = findOwnerRepoIssue(ownerName, repoName, issueNumber);
        if(issueFound == null){
            return null;
        }
        if(issue.getTitle() != null){
            issueFound.setTitle(issue.getTitle());
        }
        if(issue.getBody() != null){
            issueFound.setBody(issue.getBody());
        }
        return issueFound;
    }

    @Override
    public List<Repository> findRepositoriesByOwnerName(String name) {
        List<Repository> repositoryList = Lists.newArrayList();
        for(Issue issue: issues){
            if(issue.getRepository().getOwner().getLogin().equals(name)){
                repositoryList.add(issue.getRepository());
            }
        }
        return repositoryList;
    }

    private static List<Issue> populateDummyIssues(){
        List<Issue> issues = Lists.newArrayList();
        User user = new User(0L,
                "Pepe",
                "http://www.translations.com/sites/default/files/TP_Stacked_CMYK%20(1)_0.png",
                "",
                "",
                "User",
                false);
        User user2 = new User(1L,
                "Felipe",
                "http://www.translations.com/sites/default/files/TP_Stacked_CMYK%20(1)_0.png",
                "",
                "",
                "User",
                false);
        Owner owner1 = new Owner(0L,
                "Google",
                "https://google.com");
        Owner owner2 = new Owner(1L,
                "Annonymous",
                "https://anonymous.com");
        Repository repository1 = new Repository(
                0L,
                "chromeless",
                "Bitbucket-chromeless",
                "Automate integration",
                false,
                "https://chromeless",
                owner2);
        Repository repository2 = new Repository(
                1L,
                "tensorflow",
                "Bitbucket-tensorflow",
                "Deep learning framework",
                false,
                "https://tensorflow",
                owner1);

        Issue issue1 = new Issue(counter.incrementAndGet(),
                "",
                123L,
                "",
                "Not working",
                "The program is not working",
                user,
                repository1);
        Issue issue2 = new Issue(counter.incrementAndGet(),
                "",
                124L,
                "",
                "All is wrong",
                "When I enter to the program, It crashes",
                user2,
                repository2);

        issues.add(issue1);
        issues.add(issue2);

        return issues;
    }
}
