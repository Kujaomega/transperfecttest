FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE

RUN mkdir -p /usr/app
RUN mkdir -p /usr/app/config
WORKDIR /usr/app

COPY target/issue-manager-1.0.jar /usr/app/issue-manager-1.0.jar
COPY src/main/resources/production/application.yml /usr/app/config/
RUN ls
CMD ["java", "-jar", "issue-manager-1.0.jar", "-Dconfiguration.file=config/application.yml"]